class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :user_id
      t.integer :duration
      t.integer :loan_type
      t.date :start
      t.integer :amount

      t.timestamps
    end
  end
end
