class AddBirthdateNameCountryToUsers < ActiveRecord::Migration
  def change
    add_column :users, :birthdate, :date
    add_column :users, :name, :string
    add_column :users, :country, :string
  end
end
