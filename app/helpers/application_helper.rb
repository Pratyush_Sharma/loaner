module ApplicationHelper

	def AmountDue(loan)
		r= 10/12/100
		sum = Payment.where("loan_id = ?",loan.id).sum(:amount)

		monthlyEmi =  loan.amount * r *((1 + r)**n)/((1 + r)**n - 1)

		time = TimeDifference.between(loan.id, Time.now).in_months.to_i
		if monthlyEmi*time <sum
			return monthlyEmi-sum
		else
			return 0

		end

	end
end
